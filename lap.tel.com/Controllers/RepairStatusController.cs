﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lap.tel.com.Data;
using lap.tel.com_project.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using lap.tel.com.Models;

namespace lap.tel.com.Controllers
{
    [Authorize]
    public class RepairStatusController : Controller
    {
        private readonly laptelcomContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<RepairStatusController> _logger;

        public RepairStatusController(laptelcomContext context, UserManager<AppUser> userManager, ILogger<RepairStatusController> logger)
        {
            _context = context;
            _userManager = userManager;
            _logger = logger;
        }

        // GET: RepairStatus
        public async Task<IActionResult> Index()
        {
            var userId = _userManager.GetUserId(User);
            var repairStatuses = _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User), "Administrator").Result
                ? _context.RepairStatus.Include(r => r.Device).Include(r => r.RepairType)
                : _context.RepairStatus.Include(r => r.Device).Include(r => r.RepairType).Where(r => r.UserId == userId);

            return View(await repairStatuses.ToListAsync());
        }

        // GET: RepairStatus/Create
        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            ViewData["DeviceId"] = new SelectList(_context.Device, "DeviceId", "Marka");
            ViewData["RepairTypeId"] = new SelectList(_context.RepairType, "RepairTypeId", "RepairName");
            return View();
        }

        // POST: RepairStatus/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        // POST: RepairStatus/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create([Bind("RepairStatusId,DeviceId,RepairTypeId,Status,DateSubmitted,DateCompleted,UserId")] RepairStatus repairStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(repairStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DeviceId"] = new SelectList(_context.Device, "DeviceId", "Marka", repairStatus.DeviceId);
            ViewData["RepairTypeId"] = new SelectList(_context.RepairType.Where(rt => !rt.IsDeleted), "RepairTypeId", "RepairName", repairStatus.RepairTypeId);
            return View(repairStatus);
        }


        // GET: RepairStatus/Edit/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.RepairStatus == null)
            {
                _logger.LogError("Edit: ID is null or _context.RepairStatus is null");
                return NotFound();
            }

            var repairStatus = await _context.RepairStatus.FindAsync(id);
            if (repairStatus == null)
            {
                _logger.LogError("Edit: RepairStatus not found for ID {Id}", id);
                return NotFound();
            }

            var viewModel = new RepairStatusEditViewModel
            {
                RepairStatusId = repairStatus.RepairStatusId,
                DeviceId = repairStatus.DeviceId,
                Status = repairStatus.Status,
                UserId = repairStatus.UserId,
                DateSubmitted = repairStatus.DateSubmitted
            };

            return View(viewModel);
        }

        // POST: RepairStatus/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(RepairStatusEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var repairStatus = await _context.RepairStatus.FindAsync(viewModel.RepairStatusId);
            if (repairStatus == null)
            {
                _logger.LogError("Edit POST: RepairStatus not found for ID {Id}", viewModel.RepairStatusId);
                return NotFound();
            }

            repairStatus.Status = viewModel.Status;
            if (viewModel.Status == RepairStatusEnum.RepairCompleted)
            {
                repairStatus.DateCompleted = DateTime.Now;
                repairStatus.IsArchived = true;
            }

            try
            {
                _context.Update(repairStatus);
                await _context.SaveChangesAsync();
                _logger.LogInformation("Edit POST: Successfully updated RepairStatus with ID {Id}", viewModel.RepairStatusId);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.LogError("Edit POST: Concurrency error: {Message}", ex.Message);
                throw;
            }

            return RedirectToAction("RepairStatus", "Admin");
        }

        private bool RepairStatusExists(int id)
        {
            return (_context.RepairStatus?.Any(e => e.RepairStatusId == id)).GetValueOrDefault();
        }

        // GET: RepairStatus/Delete/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.RepairStatus == null)
            {
                return NotFound();
            }

            var repairStatus = await _context.RepairStatus
                .Include(r => r.Device)
                .Include(r => r.RepairType)
                .FirstOrDefaultAsync(m => m.DeviceId == id);
            if (repairStatus == null)
            {
                return NotFound();
            }

            return View(repairStatus);
        }

        // POST: RepairStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var repairStatus = await _context.RepairStatus.Include(rs => rs.Device).FirstOrDefaultAsync(rs => rs.DeviceId == id);
            if (repairStatus != null)
            {
                // Usuń Device przed usunięciem RepairStatus
                if (repairStatus.Device != null)
                {
                    _context.Device.Remove(repairStatus.Device);
                }
                _context.RepairStatus.Remove(repairStatus);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("RepairStatus", "Admin");
        }

        public IActionResult RepairSubmitted(int id)
        {
            {
                var repairStatus = _context.RepairStatus.Find(id);
                if (repairStatus == null)
                {
                    return NotFound();
                }

                return View(repairStatus);
            }
        }
    }
}
