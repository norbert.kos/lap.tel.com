﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lap.tel.com.Data;
using lap.tel.com_project.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using lap.tel.com.Models;

namespace lap.tel.com.Controllers
{
    [Authorize]
    public class DevicesController : Controller
    {
        private readonly laptelcomContext _context;
        private readonly UserManager<AppUser> _userManager;

        public DevicesController(laptelcomContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Devices/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Device == null)
            {
                return NotFound();
            }

            var device = await _context.Device
                .Include(d => d.RepairStatus)
                .Include(d => d.RepairType)
                .FirstOrDefaultAsync(m => m.DeviceId == id);
            if (device == null)
            {
                return NotFound();
            }

            return View(device);
        }

        // GET: Devices/Create
        public IActionResult Create()
        {
            ViewData["RepairTypeId"] = new SelectList(_context.RepairType.Where(rt => !rt.IsDeleted), "RepairTypeId", "RepairName");
            return View();
        }

        // POST: Devices/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DeviceId,Marka,Model,SerialNr,Accessory,RepairTypeId,Description,IsFlooded,IsImportantData,UserId")] Device device)
        {
            var userId = _userManager.GetUserId(User);

            if (ModelState.IsValid)
            {
                var repairType = await _context.RepairType.FindAsync(device.RepairTypeId);
                device.UserId = userId;

                _context.Add(device);
                await _context.SaveChangesAsync();

                // Teraz utwórz RepairStatus z poprawnym DeviceId
                var repairStatus = new RepairStatus
                {
                    DeviceId = device.DeviceId,  // Ustaw DeviceId z nowo utworzonego Device
                    RepairTypeId = device.RepairTypeId,
                    Status = RepairStatusEnum.NewRequest, // Domyślny status, może być modyfikowany zależnie od potrzeb
                    UserId = userId,
                    DateSubmitted = DateTime.Now
                };

                _context.Add(repairStatus);
                await _context.SaveChangesAsync();


                if (await _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User), "Administrator"))
                {
                    return RedirectToAction("Dashboard", "Admin");
                }
                else
                {
                    return RedirectToAction("RepairSubmitted" ,"Home");
                }

            }
            ViewData["RepairTypeId"] = new SelectList(_context.RepairType.Where(rt => !rt.IsDeleted), "RepairTypeId", "RepairName", device.RepairTypeId);
            return View(device);
        }


        // GET: Devices/Edit/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Device == null)
            {
                return NotFound();
            }

            var device = await _context.Device.FindAsync(id);
            if (device == null)
            {
                return NotFound();
            }
            ViewData["RepairTypeId"] = new SelectList(_context.RepairType, "RepairTypeId", "RepairName", device.RepairTypeId);
            return View(device);
        }

        // POST: Devices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int id, [Bind("DeviceId,Marka,Model,SerialNr,Accessory,RepairTypeId,Description,IsFlooded,IsImportantData")] Device device)
        {
            if (id != device.DeviceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(device);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeviceExists(device.DeviceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Devices", new { id = device.DeviceId });
            }
            ViewData["RepairTypeId"] = new SelectList(_context.RepairType, "RepairTypeId", "RepairName", device.RepairTypeId);
            return View(device);
        }
        private bool DeviceExists(int id)
        {
          return (_context.Device?.Any(e => e.DeviceId == id)).GetValueOrDefault();
        }
    }
}
