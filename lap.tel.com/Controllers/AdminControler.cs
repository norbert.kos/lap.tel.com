﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using lap.tel.com.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using lap.tel.com.Data;

namespace lap.tel.com.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private readonly laptelcomContext _context;
        private readonly UserManager<AppUser> _userManager;

        public AdminController(UserManager<AppUser> userManager, laptelcomContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public IActionResult Dashboard()
        {
            return View();
        }

        public async Task<IActionResult> UserList()
        {
            var users = await _userManager.Users.ToListAsync();
            var nonAdminUsers = new List<AppUser>();

            foreach (var user in users)
            {
                var roles = await _userManager.GetRolesAsync(user);
                if (!roles.Contains("Administrator"))
                {
                    nonAdminUsers.Add(user);
                }
            }

            return View(nonAdminUsers);
        }

        public async Task<IActionResult> UserDetails(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        public async Task<IActionResult> UserDetails_sec(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        public async Task<IActionResult> UserDelete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        [HttpPost, ActionName("UserDelete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserDeleteEx(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var result = await _userManager.DeleteAsync(user);
            if (!result.Succeeded)
            {
                // Handle errors here
                ModelState.AddModelError(string.Empty, "Error deleting user");
                return View(user);
            }

            return RedirectToAction(nameof(UserList));
        }

        public async Task<IActionResult> RepairStatus()
        {
            var userId = _userManager.GetUserId(User);
            var repairStatuses = _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User), "Administrator").Result
                ? _context.RepairStatus.Include(r => r.Device).Include(r => r.RepairType).Include(rs => rs.User).Where(r => !r.IsArchived)
                : _context.RepairStatus.Include(r => r.Device).Include(r => r.RepairType).Include(rs => rs.User).Where(r => r.UserId == userId && !r.IsArchived);

            return View(await repairStatuses.ToListAsync());
        }

        public async Task<IActionResult> RepairArchiwe()
        {
            var userId = _userManager.GetUserId(User);
            var repairStatuses = _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User), "Administrator").Result
                ? _context.RepairStatus.Include(r => r.Device).Include(r => r.RepairType).Include(rs => rs.User).Where(r => r.IsArchived)
                : _context.RepairStatus.Include(r => r.Device).Include(r => r.RepairType).Include(rs => rs.User).Where(r => r.UserId == userId && r.IsArchived);

            return View(await repairStatuses.ToListAsync());
        }
    }
}
