﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using lap.tel.com.Data;
using lap.tel.com_project.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using lap.tel.com.Models;

namespace lap.tel.com.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RepairTypesController : Controller
    {
        private readonly laptelcomContext _context;
        private readonly UserManager<AppUser> _userManager;

        public RepairTypesController(laptelcomContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: RepairTypes
        public async Task<IActionResult> Index()
        {
            // Filtrowanie usuniętych typów napraw
            var repairTypes = await _context.RepairType
                .Where(rt => !rt.IsDeleted)
                .ToListAsync();
            return View(repairTypes);
        }

        // GET: RepairTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RepairTypes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RepairTypeId,RepairName,RepairCost,RepairDescription,UserId")] RepairType repairType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(repairType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(repairType);
        }

        // GET: RepairTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repairType = await _context.RepairType.FindAsync(id);
            if (repairType == null)
            {
                return NotFound();
            }
            return View(repairType);
        }

        // POST: RepairTypes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RepairTypeId,RepairName,RepairCost,RepairDescription,UserId,IsDeleted")] RepairType repairType)
        {
            if (id != repairType.RepairTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(repairType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RepairTypeExists(repairType.RepairTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(repairType);
        }

        // GET: RepairTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repairType = await _context.RepairType
                .FirstOrDefaultAsync(m => m.RepairTypeId == id);
            if (repairType == null)
            {
                return NotFound();
            }

            return View(repairType);
        }

        // POST: RepairTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var repairType = await _context.RepairType.FindAsync(id);
            if (repairType != null)
            {
                if (repairType.RepairName == "Inne")
                {
                    // Możesz dodać komunikat dla użytkownika informujący, że tego typu nie można usunąć
                    TempData["Error"] = "Typ naprawy 'Inne' nie może być usunięty.";
                    return RedirectToAction(nameof(Index));
                }
                repairType.IsDeleted = true;  // Oznacz jako usunięty
                _context.Update(repairType);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RepairTypeExists(int id)
        {
            return _context.RepairType.Any(e => e.RepairTypeId == id);
        }
    }
}
