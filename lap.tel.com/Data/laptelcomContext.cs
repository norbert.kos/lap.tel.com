﻿using lap.tel.com.Models;
using lap.tel.com_project.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace lap.tel.com.Data
{
    public class laptelcomContext : IdentityDbContext<AppUser>
    {
        public laptelcomContext(DbContextOptions<laptelcomContext> options)
            : base(options)
        {
        }

        public DbSet<RepairType> RepairType { get; set; }
        public DbSet<Device> Device { get; set; }
        public DbSet<RepairStatus> RepairStatus { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Device>()
                .HasOne(d => d.RepairStatus)
                .WithOne(rs => rs.Device)
                .HasForeignKey<RepairStatus>(rs => rs.DeviceId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Device>()
                .HasOne(d => d.User)
                .WithMany(u => u.Devices)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            // Inne konfiguracje ASP.NET Identity, jeśli potrzebne
            modelBuilder.Entity<IdentityUserLogin<string>>().HasKey(l => new { l.LoginProvider, l.ProviderKey });
            modelBuilder.Entity<IdentityUserRole<string>>().HasKey(r => new { r.UserId, r.RoleId });
            modelBuilder.Entity<IdentityUserToken<string>>().HasKey(t => new { t.UserId, t.LoginProvider, t.Name });
        }
    }
}