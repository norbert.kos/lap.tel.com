﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace lap.tel.com.Migrations
{
    /// <inheritdoc />
    public partial class typesdel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "RepairType",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "RepairType");
        }
    }
}
