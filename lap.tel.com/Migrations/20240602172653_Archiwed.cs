﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace lap.tel.com.Migrations
{
    /// <inheritdoc />
    public partial class Archiwed : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "RepairStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "RepairStatus");
        }
    }
}
