using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using lap.tel.com.Data;
using lap.tel.com.Models;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using lap.tel.com_project.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");

// Konfiguruj obsługiwane kultury
var supportedCultures = new List<CultureInfo>
{
    new CultureInfo("pl-PL")
};

builder.Services.Configure<RequestLocalizationOptions>(options =>
{
    options.DefaultRequestCulture = new RequestCulture("pl-PL");
    options.SupportedCultures = supportedCultures;
    options.SupportedUICultures = supportedCultures;
});

builder.Services.AddDbContext<laptelcomContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("laptelcomContext") ?? throw new InvalidOperationException("Connection string 'laptelcomContext' not found."))
           .EnableSensitiveDataLogging());

builder.Services.AddDefaultIdentity<AppUser>(options =>
{
    options.SignIn.RequireConfirmedAccount = true;
})
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<laptelcomContext>()
    .AddDefaultTokenProviders()
    .AddDefaultUI();

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    async Task SeedRepairTypeAsync(IServiceProvider services)
    {
        var context = services.GetRequiredService<laptelcomContext>();
        var logger = services.GetRequiredService<ILogger<Program>>();

        var existingType = context.RepairType.FirstOrDefault(rt => rt.RepairName == "Inne");
        if (existingType == null)
        {
            logger.LogInformation("Adding new RepairType: Inne");
            context.RepairType.Add(new RepairType { RepairName = "Inne" });
            await context.SaveChangesAsync();
        }
        else
        {
            logger.LogInformation("RepairType already exists");
        }
    }

    var services = scope.ServiceProvider;
    try
    {
        var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
        var userManager = services.GetRequiredService<UserManager<AppUser>>();
        await SeedRolesAsync(roleManager);
        await SeedAdminAsync(userManager);
        await SeedRepairTypeAsync(services);
    }
    catch (Exception ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "An error occurred while seeding the database.");
    }
}

// Konfiguracja potoku HTTP
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseRequestLocalization();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();

async Task SeedRolesAsync(RoleManager<IdentityRole> roleManager)
{
    if (!await roleManager.RoleExistsAsync("Administrator"))
    {
        await roleManager.CreateAsync(new IdentityRole("Administrator"));
    }
    if (!await roleManager.RoleExistsAsync("User"))
    {
        await roleManager.CreateAsync(new IdentityRole("User"));
    }
}

async Task SeedAdminAsync(UserManager<AppUser> userManager)
{
    var adminUser = new AppUser
    {
        UserName = "admin@admin.com",
        Email = "admin@admin.com",
        EmailConfirmed = true,
        Imie = "Admin",
        Nazwisko = "Administrator",
        NumerTel = "111111111",
        Adres = "123 Admin Street",
        KodPocztowy = "00-000",
        Miejscowosc = "Admin City",
        DataUrodzenia = new DateTime(1980, 1, 1)  // Przykładowa data urodzenia
    };

    var user = await userManager.FindByEmailAsync(adminUser.Email);
    if (user == null)
    {
        var createAdminResult = await userManager.CreateAsync(adminUser, "Admin123!"); // Ustaw silne has�o zgodne z wymaganiami polityki
        if (createAdminResult.Succeeded)
        {
            await userManager.AddToRoleAsync(adminUser, "Administrator");
        }
    }
}