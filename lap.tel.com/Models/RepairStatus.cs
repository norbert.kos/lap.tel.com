﻿using System.ComponentModel.DataAnnotations;
using lap.tel.com.Models;

namespace lap.tel.com_project.Models
{
    public enum RepairStatusEnum
    {
        [Display(Name = "Nowe zgłoszenie")]
        NewRequest,

        [Display(Name = "Otrzymano przesyłkę")]
        PackageReceived,

        [Display(Name = "W trakcie diagnozy")]
        DiagnosticsInProgress,

        [Display(Name = "W trakcie naprawy")]
        RepairInProgress,

        [Display(Name = "Odesłano przesyłkę")]
        PackageSent,

        [Display(Name = "Naprawa zakończona")]
        RepairCompleted
    }
    public class RepairStatus
    {
        public int RepairStatusId { get; set; }

        [Display(Name = "Numer zgłoszenia"), Required]
        public int DeviceId { get; set; }

        [Display(Name = "Numer zgłoszenia")]
        public virtual Device? Device { get; set; }

        [Display(Name = "Wybierz typ naprawy"), Required]
        public int RepairTypeId { get; set; }

        [Display(Name = "Typ naprawy")]
        public virtual RepairType? RepairType { get; set; }

        [Display(Name = "Status naprawy"), Required]
        public RepairStatusEnum Status { get; set; }

        [Display(Name = "Data zgłoszenia"), Required]
        public DateTime DateSubmitted { get; set; }

        [Display(Name = "Data zakończenia")]
        public DateTime? DateCompleted { get; set; }
        public bool IsArchived { get; set; }

        public string? UserId { get; set; }
        public virtual AppUser? User { get; set; }
    }
}