﻿using lap.tel.com_project.Models;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace lap.tel.com.Models
{
    public class AppUser : IdentityUser
    {
        [Required(ErrorMessage = "Imię jest wymagane.")]
        [StringLength(50, ErrorMessage = "Wprowadź poprawne imię.")]
        public string Imie { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane.")]
        [StringLength(50, ErrorMessage = "Wprowadź poprawne nazwisko.")]
        public string Nazwisko { get; set; }

        [Required(ErrorMessage = "Numer telefonu jest wymagany.")]
        [StringLength(50, ErrorMessage = "Wprowadź poprawne nazwisko.")]
        public string NumerTel { get; set; }

        [Required(ErrorMessage = "Adres zamieszkania jest wymagany.")]
        [StringLength(100, ErrorMessage = "Wprowadź poprawny adres.")]
        public string Adres { get; set; }

        [Required(ErrorMessage = "Kod pocztowy jest wymagany.")]
        [RegularExpression(@"^\d{2}-\d{3}$", ErrorMessage = "Kod pocztowy musi być w formacie XX-XXX.")]
        public string KodPocztowy { get; set; }

        [Required(ErrorMessage = "Miejscowość jest wymagana.")]
        [StringLength(100, ErrorMessage = "Wprowadź poprawną miejscowość")]
        public string Miejscowosc { get; set; }

        [Required(ErrorMessage = "Data urodzenia jest wymagana.")]
        [DataType(DataType.Date)]
        [Display(Name = "Data urodzenia")]
        public DateTime DataUrodzenia { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
    }
}
