﻿using System.ComponentModel.DataAnnotations;
using lap.tel.com.Models;

namespace lap.tel.com_project.Models
{
    public class Device
    {
        public int DeviceId { get; set; }

        [Required, StringLength(50)]
        public string Marka { get; set; }

        [Required, StringLength(50)]
        public string Model { get; set; }

        [Display(Name = "Numer SN/IMEI"), Required, StringLength(50)]
        public string SerialNr { get; set; }

        [Display(Name = "Akcesoria")]
        public string Accessory { get; set; }

        [Display(Name = "Wybierz typ naprawy")]
        public int RepairTypeId { get; set; }

        [Display(Name = "Typ naprawy")]
        public virtual RepairType? RepairType { get; set; }

        [Display(Name = "Opis uszkodzenia"), StringLength(500)]
        public string Description { get; set; }

        [Display(Name = "Czy zalany?"), Required]
        public bool IsFlooded { get; set; }

        [Display(Name = "Czy ważne dane?"), Required]
        public bool IsImportantData { get; set; }

        public virtual RepairStatus? RepairStatus { get; set; }
        public string? UserId { get; set; }
        public virtual AppUser? User { get; set; }
    }
}