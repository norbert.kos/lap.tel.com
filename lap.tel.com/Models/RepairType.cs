﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using lap.tel.com.Models;

namespace lap.tel.com_project.Models
{
    public class RepairType
    {
        public int RepairTypeId { get; set; }
        [Display(Name = "Nazwa"), Required]
        public string RepairName { get; set; }
        [Display(Name = "Koszt naprawy")]
        public int? RepairCost { get; set; }
        [Display(Name="Opis naprawy"), StringLength(100)]
        public string? RepairDescription { get; set; }
        public bool IsDeleted { get; set; }
        public string? UserId { get; set; }
        public virtual AppUser? User { get; set; }
    }
}
