﻿using lap.tel.com_project.Models;

namespace lap.tel.com.Models
{
    public class RepairStatusEditViewModel
    {
        public int RepairStatusId { get; set; }
        public int DeviceId { get; set; }
        public RepairStatusEnum Status { get; set; }
        public int RepairTypeId { get; set; }
        public DateTime DateSubmitted { get; set; }  // Przekazuj jako ukryte pole jeśli nie edytowalne
        public string UserId { get; set; }  // Przekazuj jako ukryte pole jeśli nie edytowalne
    }
}
