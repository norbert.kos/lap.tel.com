# lap.tel.com



## Getting started

Aby włączyć projekt wpisz w konsoli gita polecenie:
"git clone https://gitlab.com/norbert.kos/lap.tel.com.git"

Utworzy się repozytorium z projektem. Po wejściu w projekt w Visual Studio,
wpisz w konsoli pakietów NuGet polecenie:
"update database"

Uruchom projekt w Visual Studio.

Aby zalogować się na konto administratora użyj następujących danych logowania:
Email: "admin@admin.com"
Hasło: "Admin123!"

